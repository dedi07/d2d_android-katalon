import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button menu explore'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button menu explore'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Explore - menu event'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Explore - menu event'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - text Event'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), 0)

Mobile.comment('cek detail event')

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Event/Event - button card event'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button share'), GlobalVariable.g_timeout)

Mobile.verifyElementVisible(findTestObject('Detail Event/Event - text event'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - Judul event'), 0)

String judul_event = Mobile.getText(findTestObject('Detail Event/Event - Judul event'), 0)

GlobalVariable.g_judul_event = judul_event

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.comment('klik bookmark ')

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Bookmark'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Detail Event/Event - button Bookmark'), 0)

if (Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - text popup add to calendar'), 5) == true) {
    Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - button popup Later'), 0)

    Mobile.tap(findTestObject('Detail Event/Bookmark - button popup Later'), 0)
}

Mobile.comment('klik add to calendar')

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Calendar'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Detail Event/Event - button Calendar'), 0)

if (Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - text popup add to calendar'), 3) == true) {
    Mobile.waitForElementPresent(findTestObject('Detail Event/Bookmark - button popup OK'), 0)

    Mobile.tap(findTestObject('Detail Event/Bookmark - button popup OK'), 0)
}

if (Mobile.waitForElementPresent(findTestObject('Detail Event/Calendar - button Batal add'), 3) == true) {
    Mobile.tap(findTestObject('Detail Event/Calendar - button Batal add'), 0)
} else {
    Mobile.tap(findTestObject('Detail Event/Calendar - button Batal add 2'), 0)
}

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Calendar'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Certificate'), 3) == true) {
    Mobile.comment('buka certificate')

    Mobile.tap(findTestObject('Detail Event/Event - button Certificate'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - download sertifikat manual'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('CME/CME - download sertifikat manual'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - toast download berhasil manual'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('CME/CME - button send email manual'), 0)

    Mobile.tap(findTestObject('CME/CME - button send email manual'), 0)

    Mobile.waitForElementPresent(findTestObject('CME/CME - toast sukses kirim email manual'), 5)

    Mobile.comment('back ke halaman detil event')

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CME/CME - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
}

if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 3) == true) {
    Mobile.comment('buka materi')

    Mobile.tap(findTestObject('Detail Event/Event - button Materi'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - button back materi'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - text Event Material'), 0)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('Detail Event/Materi - button back materi'), 0)

    Mobile.comment('back ke halaman detil event')

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
}

Mobile.comment('share')

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button share'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Detail Event/Event - button share'), 0)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), GlobalVariable.g_timeout)

Mobile.comment('kembali ke list event')

Mobile.tap(findTestObject('Detail Event/Event - button back'), 0)

Mobile.comment('Buka Tab bookmark')

println(GlobalVariable.g_judul_event)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - Tab Bookmark'), 0)

Mobile.tap(findTestObject('Explore/Event/Event - Tab Bookmark'), 0)

Mobile.comment('buka event di tab bookmark')

if (Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), 3) == true) {
    Mobile.tap(findTestObject('Explore/Event/Event - button card event'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - Judul event'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Bookmark'), 0)

    Mobile.tap(findTestObject('Detail Event/Event - button Bookmark'), 0)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detail Event/Event - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - Tab Joined'), 0)
}

Mobile.comment('Buka tab Joined')

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - Tab Joined'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Event/Event - Tab Joined'), 0)

if (Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), 3) == true) {
    Mobile.tap(findTestObject('Explore/Event/Event - button card event'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - Judul event'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Bookmark'), 0)

    if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Certificate'), 3) == true) {
        Mobile.comment('buka certificate')

        Mobile.tap(findTestObject('Detail Event/Event - button Certificate'), 0)

        if (Mobile.waitForElementPresent(findTestObject('Detail Event/Popup - text popup sertifikat'), 3) == true) {
            Mobile.setText(findTestObject('Detail Event/Popup - input nama gelar sertifikat'), GlobalVariable.fullname_prod, 
                0)

            Mobile.tap(findTestObject('Detail Event/Popup - submit nama sertifikat'), 0)

            Mobile.waitForElementPresent(findTestObject('Detail Event/Popup - confirm sertifikat name'), 0)

            Mobile.waitForElementPresent(findTestObject('Detail Event/Popup - button yes confirm sertifikat'), 0)

            Mobile.tap(findTestObject('Detail Event/Popup - button yes confirm sertifikat'), 0)

            Mobile.waitForElementPresent(findTestObject('Detail Event/Popup - button close done'), 0)

            Mobile.tap(findTestObject('Detail Event/Popup - button close done'), 0)

            if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Certificate'), 3) == true) {
                Mobile.comment('buka certificate')

                Mobile.tap(findTestObject('Detail Event/Event - button Certificate'), 0)

                Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

                Mobile.waitForElementPresent(findTestObject('CME/CME - download sertifikat manual'), GlobalVariable.g_timeout)

                Mobile.tap(findTestObject('CME/CME - download sertifikat manual'), 0)

                Mobile.waitForElementPresent(findTestObject('CME/CME - toast download berhasil manual'), GlobalVariable.g_timeout)

                Mobile.waitForElementPresent(findTestObject('CME/CME - button send email manual'), 0)

                Mobile.tap(findTestObject('CME/CME - button send email manual'), 0)

                Mobile.waitForElementPresent(findTestObject('CME/CME - toast sukses kirim email manual'), 5)

                Mobile.comment('back ke halaman detil event')

                Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

                Mobile.tap(findTestObject('CME/CME - button back'), 0)

                Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
            }
            
            if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 3) == true) {
                Mobile.comment('buka materi')

                Mobile.tap(findTestObject('Detail Event/Event - button Materi'), 0)

                Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - button back materi'), GlobalVariable.g_timeout)

                Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - text Event Material'), 0)

                Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

                Mobile.tap(findTestObject('Detail Event/Materi - button back materi'), 0)

                Mobile.comment('back ke halaman detil event')

                Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
            }
            
            Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), 0)

            Mobile.tap(findTestObject('Detail Event/Event - button back'), 0)

            Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button search'), 0)
        }
        
        Mobile.waitForElementPresent(findTestObject('CME/CME - button back'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - download sertifikat manual'), GlobalVariable.g_timeout)

        Mobile.tap(findTestObject('CME/CME - download sertifikat manual'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - toast download berhasil manual'), GlobalVariable.g_timeout)

        Mobile.waitForElementPresent(findTestObject('CME/CME - button send email manual'), 0)

        Mobile.tap(findTestObject('CME/CME - button send email manual'), 0)

        Mobile.waitForElementPresent(findTestObject('CME/CME - toast sukses kirim email manual'), 5)

        Mobile.comment('back ke halaman detil event')

        Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CME/CME - button back'), 0)

        Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
    }
    
    if (Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 3) == true) {
        Mobile.comment('buka materi')

        Mobile.tap(findTestObject('Detail Event/Event - button Materi'), 0)

        Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - button back materi'), GlobalVariable.g_timeout)

        Mobile.waitForElementPresent(findTestObject('Detail Event/Materi - text Event Material'), 0)

        Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Detail Event/Materi - button back materi'), 0)

        Mobile.comment('back ke halaman detil event')

        Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button Materi'), 0)
    }
    
    Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), 0)

    Mobile.tap(findTestObject('Detail Event/Event - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button search'), 0)
}

Mobile.comment('search event')

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button search'), 0)

Mobile.tap(findTestObject('Explore/Event/Event - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - input Search event'), 0)

Mobile.setText(findTestObject('Explore/Event/Event - input Search event'), GlobalVariable.g_judul_event, 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button card event'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Event/Event - button card event'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - button back'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Detail Event/Event - Judul event'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Detail Event/Event - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button close cari event'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Event/Event - button close cari event'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - button filter'), 0)

Mobile.tap(findTestObject('Explore/Event/Event - button filter'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - filter januari'), 0)

Mobile.tap(findTestObject('Explore/Event/Event - filter januari'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Explore/Event/Event - filter januari'), 0)

Mobile.pressBack()

Mobile.waitForElementPresent(findTestObject('Explore/Event/Event - text Event'), GlobalVariable.g_timeout)

