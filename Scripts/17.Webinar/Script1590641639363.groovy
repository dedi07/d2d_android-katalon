import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button menu webinar'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button menu webinar'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - text webinar'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - card webinar'), 0)

Mobile.comment('explore detail webinar')

Mobile.tap(findTestObject('Webinar/Webinar - card webinar'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - text judul webinar'), 0)

judul_webinar = Mobile.getText(findTestObject('Webinar/Detail webinar/Webinar - text judul webinar'), 0)

GlobalVariable.g_judul_webinar = judul_webinar

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - button fullscreen'), 0)

Mobile.comment('like webinar')

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - button like'), 0)

Mobile.tap(findTestObject('Webinar/Detail webinar/Webinar - button like'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Webinar/Detail webinar/Webinar - button like'), 0)

Mobile.comment('share webinar')

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - button share'), 0)

Mobile.tap(findTestObject('Webinar/Detail webinar/Webinar - button share'), 0)

Mobile.delay(8, FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.comment('show description & hide ')

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - button show desc'), 0)

Mobile.tap(findTestObject('Webinar/Detail webinar/Webinar - button show desc'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.comment('kembali ke list webinar')

Mobile.tap(findTestObject('Webinar/Detail webinar/Webinar - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - button search'), 0)

Mobile.comment('lakukan search')

Mobile.tap(findTestObject('Webinar/Webinar - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - input search webinar'), 0)

Mobile.setText(findTestObject('Webinar/Webinar - input search webinar'), GlobalVariable.g_judul_webinar, 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - card webinar'), 0)

Mobile.tap(findTestObject('Webinar/Webinar - card webinar'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - text judul webinar'), 0)

Mobile.comment('kembali ke list webinar dari search')

Mobile.waitForElementPresent(findTestObject('Webinar/Detail webinar/Webinar - button back'), 0)

Mobile.tap(findTestObject('Webinar/Detail webinar/Webinar - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - button close search'), 0)

Mobile.tap(findTestObject('Webinar/Webinar - button close search'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - text webinar'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Webinar/Webinar - card webinar'), 0)

