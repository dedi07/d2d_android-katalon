import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

def pasien = false

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button PMM'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button PMM'), 0)

Mobile.comment('Masuk halaman PMM')

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - text Kelola Teman Diabetes'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - button back'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - button show QR'), GlobalVariable.g_timeout)

Mobile.comment('show QR')

Mobile.tap(findTestObject('PMM detail/PMM - button show QR'), 0)

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - text Pairing to patient'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - close QR popup'), 0)

Mobile.tap(findTestObject('PMM detail/PMM - close QR popup'), 0)

Mobile.comment('handle pasien')

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - text Tab Pasien'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('PMM detail/PMM - text Tab Pasien'), 0)

if (Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - text belum ada pasien'), 5, FailureHandling.OPTIONAL) == 
true) {
    println('Belum Pairing dengan Pasien')

    pasien = false
} else {
    println('Buat flow handle test case pasien disini')

    pasien = true
}

Mobile.comment('handle notifikasi')

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - text Tab Notifikasi'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('PMM detail/PMM - text Tab Notifikasi'), 0)

if (Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - text belum ada notifikasi'), 5, FailureHandling.OPTIONAL) == 
true) {
    println('Belum ada Notifikasi')
} else {
    println('Buat flow handle test case notifikasi disini')
}

Mobile.comment('cari pasien')

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - button seacrh pasien'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('PMM detail/PMM - button seacrh pasien'), 0)

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - input text cari pasien'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('PMM detail/PMM - input text cari pasien'), 0)

if (pasien == true) {
    Mobile.setText(findTestObject('PMM detail/PMM - input text cari pasien'), '', 0)
}

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - button close pencarian'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('PMM detail/PMM - button close pencarian'), 0)

Mobile.waitForElementPresent(findTestObject('PMM detail/PMM - button back'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('PMM detail/PMM - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text feeds'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button scan QR'), GlobalVariable.g_timeout)

