import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

def deviceHight = Mobile.getDeviceHeight()

def deviceWidth = Mobile.getDeviceWidth()

int startX = deviceWidth / 2

int endX = startX

int startY = deviceHight * 0.90

int endY = deviceWidth

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button menu profile'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button menu profile'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - text nama user'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - upload foto profile'), 0)

nama_user = Mobile.getText(findTestObject('Profile/Profile - text nama user'), 0)

v_nama_user = nama_user

Mobile.comment('upload foto profile')

Mobile.tap(findTestObject('Profile/Profile - upload foto profile'), 0)

Mobile.waitForElementPresent(findTestObject('Extra/Foto - button take photo'), 0)

Mobile.tap(findTestObject('Extra/Foto - button take photo'), 0)

Mobile.waitForElementPresent(findTestObject('Extra/camera - tombol shutter'), 0)

Mobile.tap(findTestObject('Extra/camera - tombol shutter'), 0)

Mobile.waitForElementPresent(findTestObject('Extra/camera - button ok'), 0)

Mobile.tap(findTestObject('Extra/camera - button ok'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - text nama user'), 0)

Mobile.comment('buka halaman bookmark')

Mobile.waitForElementPresent(findTestObject('Profile/Profile - tab bookmark'), 0)

Mobile.tap(findTestObject('Profile/Profile - tab bookmark'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - text title bookmark'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - button back bookmark'), 0)

Mobile.tap(findTestObject('Profile/Profile - button back bookmark'), 0)

Mobile.comment('buka halaman download')

Mobile.waitForElementPresent(findTestObject('Profile/Profile - tab Download'), 0)

Mobile.tap(findTestObject('Profile/Profile - tab Download'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - button back download'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - text title download'), 0)

Mobile.tap(findTestObject('Profile/Profile - button back download'), 0)

Mobile.comment('buka halaman notification & kembali ke latest')

Mobile.waitForElementPresent(findTestObject('Profile/Profile - tab notification'), 0)

Mobile.tap(findTestObject('Profile/Profile - tab notification'), 0)

Mobile.delay(4, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Profile/Profile - tab profile'), 0)

Mobile.tap(findTestObject('Profile/Profile - tab profile'), 0)

Mobile.comment('save profile')

while (Mobile.waitForElementPresent(findTestObject('Profile/Profile - button save'), 2, FailureHandling.CONTINUE_ON_FAILURE) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.tap(findTestObject('Profile/Profile - button save'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.comment('explore TnC')

Mobile.waitForElementPresent(findTestObject('Profile/Profile - button more'), 0)

Mobile.tap(findTestObject('Profile/Profile - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/More - button TnC'), 0)

Mobile.tap(findTestObject('Profile/More - button TnC'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/More - text title TnC'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.comment('explore privacy policy')

Mobile.waitForElementPresent(findTestObject('Profile/Profile - button more'), 0)

Mobile.tap(findTestObject('Profile/Profile - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/More - button privacy'), 0)

Mobile.tap(findTestObject('Profile/More - button privacy'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/More - text title privacy policy'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.comment('explore contact us')

Mobile.waitForElementPresent(findTestObject('Profile/Profile - button more'), 0)

Mobile.tap(findTestObject('Profile/Profile - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/More - button contact us'), 0)

Mobile.tap(findTestObject('Profile/More - button contact us'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/More - text title Contact Us'), 0)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.comment('explore logout')

Mobile.waitForElementPresent(findTestObject('Profile/Profile - button more'), 0)

Mobile.tap(findTestObject('Profile/Profile - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/More - button logout'), 0)

Mobile.tap(findTestObject('Profile/More - button logout'), 0)

Mobile.waitForElementPresent(findTestObject('Login/Login - text welcome'), 0)

Mobile.waitForElementPresent(findTestObject('Login/Login - button login'), 0)

Mobile.waitForElementPresent(findTestObject('Login/Login - button register'), 0)

Mobile.closeApplication()

