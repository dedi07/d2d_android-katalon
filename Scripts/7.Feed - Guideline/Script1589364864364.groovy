import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

def scroll = false

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text feeds'), GlobalVariable.g_timeout)

Mobile.verifyElementVisible(findTestObject('Feeds/Feeds - button PMM'), 0)

Mobile.verifyElementVisible(findTestObject('Feeds/Feeds - button scan QR'), 0)

def deviceHight = Mobile.getDeviceHeight()

def deviceWidth = Mobile.getDeviceWidth()

int startX = deviceWidth / 2

int endX = startX

int startY = deviceWidth

int endY = deviceWidth * 0.35

//swipe up : Mobile.swipe(startX, startY, endX, endY)
//swipe down : Mobile.swipe(startX, endY, endX, startY)
if (Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text tag Guideline'), 5, FailureHandling.OPTIONAL) == false) {
    Mobile.swipe(startX, startY, endX, endY)

    Mobile.swipe(startX, startY, endX, endY)

    scroll = true

    println(scroll)
}

if (Mobile.waitForElementPresent(findTestObject('Feeds/Feed - text tag Journal'), 10, FailureHandling.OPTIONAL) == true) {
    Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text tag Guideline'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Feeds/Feeds - text tag Guideline'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - button close'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - text Guideline feeds'), GlobalVariable.g_timeout)

    Mobile.verifyElementVisible(findTestObject('Detail Guideline/Guideline - button download'), 0)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.comment('download pdf')

    Mobile.tap(findTestObject('Detail Guideline/Guideline - button download'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - File sukses download'), GlobalVariable.g_timeout)

    Mobile.comment('kembali ke list Feeds')

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - button close'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detail Guideline/Guideline - button close'), 0)

    Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text feeds'), GlobalVariable.g_timeout)

    if (scroll == true) {
        Mobile.swipe(startX, endY, endX, startY)

        Mobile.swipe(startX, endY, endX, startY)
    }
} else {
    println('Skip Karena Guideline tidak ditemukan di Feeds')
}

