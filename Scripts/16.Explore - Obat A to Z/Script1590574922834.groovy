import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

def deviceHight = Mobile.getDeviceHeight()

def deviceWidth = Mobile.getDeviceWidth()

int startX = deviceWidth / 2

int endX = startX

int startY = deviceWidth

int endY = deviceWidth * 0.35

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button menu explore'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button menu explore'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Explore - obat A-Z'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Explore - obat A-Z'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - text obat A to Z'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - list generic'), 0)

Mobile.comment('pilih alphabet')

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - alphabet E'), 0)

Mobile.tap(findTestObject('Explore/Obat A to Z/Obat - alphabet E'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - list generic'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - alphabet A'), 0)

Mobile.tap(findTestObject('Explore/Obat A to Z/Obat - alphabet A'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - list generic'), 0)

Mobile.comment('masuk ke detail obat')

Mobile.tap(findTestObject('Explore/Obat A to Z/Obat - card list obat'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Obat/Obat - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Obat/Obat - text penggunaan obat'), 0)

while (Mobile.waitForElementPresent(findTestObject('Detail Obat/Obat - text related'), 2, FailureHandling.CONTINUE_ON_FAILURE) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.verifyElementExist(findTestObject('Detail Obat/Obat - text related'), 0)

Mobile.comment('kembali ke list obat A to Z')

Mobile.tap(findTestObject('Detail Obat/Obat - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - button Cari obat'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - text obat A to Z'), 0)

Mobile.comment('cari obat')

Mobile.tap(findTestObject('Explore/Obat A to Z/Obat - button Cari obat'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - input cari obat'), 0)

Mobile.setText(findTestObject('Explore/Obat A to Z/Obat - input cari obat'), v_obat, 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - card list obat'), 0)

Mobile.tap(findTestObject('Explore/Obat A to Z/Obat - card list obat'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Obat/Obat - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Obat/Obat - text penggunaan obat'), 0)

Mobile.comment('kembali ke list obat A to Z dari pencarian')

Mobile.tap(findTestObject('Detail Obat/Obat - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - button close search'), 0)

Mobile.tap(findTestObject('Explore/Obat A to Z/Obat - button close search'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Obat A to Z/Obat - text obat A to Z'), 0)

