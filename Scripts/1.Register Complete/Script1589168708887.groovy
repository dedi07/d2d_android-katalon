import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.github.javafaker.Faker as Faker
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import org.openqa.selenium.Keys as Keys

Mobile.callTestCase(findTestCase('onboarding'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.comment('create data dummy dengan faker')

Faker faker = new Faker()

String name = faker.name().firstName()

println(name)

String namadepan = (name + ' ') + 'Test'

String namadepan2 = name + 'Test'

println(namadepan)

GlobalVariable.g_nama_regis = namadepan

println(namadepan2)

String email = namadepan2 + '@yopmail.com'

println(email)

GlobalVariable.g_email_regis = email

Mobile.waitForElementPresent(findTestObject('Login/Login - button register'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Login/Login - button register'), 0)

Mobile.waitForElementPresent(findTestObject('Register/Register - text input your data'), GlobalVariable.g_timeout, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Register/Register - input Fullname'), 0)

Mobile.setText(findTestObject('Register/Register - input Fullname'), GlobalVariable.g_nama_regis, 0)

Mobile.setText(findTestObject('Register/Register - input email'), GlobalVariable.g_email_regis, 0)

Mobile.setText(findTestObject('Register/Register - input password'), GlobalVariable.g_email_pass, 0)

Mobile.setText(findTestObject('Register/Register - input confirm password'), GlobalVariable.g_email_pass, 0)

Mobile.tap(findTestObject('Register/Register - button register'), 0)

Mobile.waitForElementPresent(findTestObject('Register/Register - text Email Verification'), GlobalVariable.g_timeout)

Mobile.verifyElementExist(findTestObject('Register/Register - text Sent Activation link'), 0)

Mobile.tap(findTestObject('Register/Register - button back email verif'), 0)

Mobile.waitForElementPresent(findTestObject('Login/Login - button login'), GlobalVariable.g_timeout)

Mobile.comment('buka chrome untuk akses email & klik aktivasi')

Mobile.startExistingApplication('com.android.chrome', FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Button expand tab'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Chrome - Button expand tab'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Add new tab'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Chrome - Add new tab'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Input Search Google'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Chrome - Input Search Google'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Input Url Bar'), GlobalVariable.g_timeout)

Mobile.setText(findTestObject('Chrome/Chrome - Input Url Bar'), 'yopmail.com' + '\\n', 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - icon header yopmail'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - input email yopmail'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Yopmail - input email yopmail'), 0)

Mobile.setText(findTestObject('Chrome/Yopmail - input email yopmail'), email + '\\n', 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - icon trash (hapus email)'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - Email yang diterima Register'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Yopmail - Email yang diterima Register'), 0)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - text Title Aktivasi Register'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Chrome/Yopmail - Link aktivasi Register'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Chrome/Yopmail - Link aktivasi Register'), 0)

if (Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Buka dengan D2D'), 10, FailureHandling.OPTIONAL) == true) {
    Mobile.waitForElementPresent(findTestObject('Chrome/Chrome - Buka dengan D2D'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Chrome/Chrome - Buka dengan D2D'), 0)
}

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

if (Mobile.waitForElementPresent(findTestObject('Login/Login - button login'), 10, FailureHandling.OPTIONAL) == false) {
    Mobile.waitForElementPresent(findTestObject('Register/Register - text Email Verification'), GlobalVariable.g_timeout)

    Mobile.pressBack()

    Mobile.waitForElementPresent(findTestObject('Login/Login - button login'), GlobalVariable.g_timeout)
}

Mobile.comment('Login dengan akun yang baru di registrasi')

Mobile.waitForElementPresent(findTestObject('Login/Login - button login'), 0)

Mobile.setText(findTestObject('Login/Login - input email'), email, 0)

Mobile.setText(findTestObject('Login/Login - input password'), GlobalVariable.g_email_pass, 0)

Mobile.tap(findTestObject('Login/Login - button login'), GlobalVariable.g_timeout)

Mobile.comment('input data setelah register')

Mobile.waitForElementPresent(findTestObject('Form Register/Form - text Input your data'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Form Register/Form - text thank you'), 0)

Mobile.tap(findTestObject('Form Register/Form - button Next'), 0)

Mobile.comment('input spesialisasi')

Mobile.waitForElementPresent(findTestObject('Form Register/Form - text whats your specialization'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Form Register/Form - pilihan spesialisasi'), 0)

Mobile.waitForElementPresent(findTestObject('Form Register/Spesialisasi - text select specialization'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Form Register/spesialisasi - text spesialis bedah anak'), 0)

Mobile.tap(findTestObject('Form Register/Spesialisasi - button save'), 0)

Mobile.waitForElementPresent(findTestObject('Form Register/Form - link add spesialisasi'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Form Register/Form - link add spesialisasi'), 0)

Mobile.waitForElementPresent(findTestObject('Form Register/Spesialisasi - text select specialization'), GlobalVariable.g_timeout)

Mobile.setText(findTestObject('Form Register/spesialisasi - input cari'), v_spesialis, 0)

Mobile.tap(findTestObject('Form Register/spesialisasi - text dokter umum'), 0)

Mobile.tap(findTestObject('Form Register/Spesialisasi - button save'), 0)

Mobile.waitForElementPresent(findTestObject('Form Register/Form - button Next di spesialis'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Form Register/Form - button Next di spesialis'), 0)

Mobile.comment('input practice location')

Mobile.waitForElementPresent(findTestObject('Form Register/Form - text whares your practice location'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Form Register/Form - pilih tempat praktek'), 0)

Mobile.waitForElementPresent(findTestObject('Form Register/location - text practice location'), GlobalVariable.g_timeout)

//String klinik = 'Test' + 
Mobile.setText(findTestObject('Form Register/location - input tempat praktek'), 'klinik' + namadepan, 0)

Mobile.tap(findTestObject('Form Register/location - button save'), 0)

Mobile.waitForElementPresent(findTestObject('Form Register/Form - button next di location'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Form Register/Form - button next di location'), 0)

Mobile.waitForElementPresent(findTestObject('Form Register/Form - text complete registration step'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Form Register/Form - button Finish'), 0)

Mobile.callTestCase(findTestCase('Popup Edukasi'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text feeds'), GlobalVariable.g_timeout)

Mobile.callTestCase(findTestCase('Logout'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

