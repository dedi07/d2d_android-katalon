import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

'kondisi untuk pakai apk prod / stag sesuai yang di setting di profile'
if (GlobalVariable.Environtment_production == true) {
    println('jalankan apk prod')

    Mobile.startApplication(GlobalVariable.g_apk_prod, true)
} else {
    println('jalankan apk staging')

    Mobile.startApplication(GlobalVariable.g_apk_stag, true)
}

Mobile.comment('Handle Update')

if (Mobile.waitForElementPresent(findTestObject('Onboarding/update - text update application'), 10, FailureHandling.OPTIONAL) == 
true) {
    Mobile.waitForElementPresent(findTestObject('Onboarding/update - button Later'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Onboarding/update - button Later'), GlobalVariable.g_timeout)
}

Mobile.waitForElementPresent(findTestObject('Onboarding/onboarding - text event'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Onboarding/onboarding - button continue'), 0)

Mobile.waitForElementPresent(findTestObject('Onboarding/onboarding - text CME'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Onboarding/onboarding - button continue'), 0)

Mobile.waitForElementPresent(findTestObject('Onboarding/onboarding - text Medicinus'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Onboarding/onboarding - button start now'), 0)

Mobile.waitForElementPresent(findTestObject('Login/Login - text welcome'), GlobalVariable.g_timeout)

