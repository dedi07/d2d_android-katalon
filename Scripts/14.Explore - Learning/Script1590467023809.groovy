import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

def deviceHight = Mobile.getDeviceHeight()

def deviceWidth = Mobile.getDeviceWidth()

int startX = deviceWidth / 2

int endX = startX

int startY = deviceWidth

int endY = deviceWidth * 0.35

Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - button menu explore'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Feeds/Feeds - button menu explore'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Explore - menu learning'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Explore - menu learning'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - text Learning'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - button card spesialis'), 0)

Mobile.comment('cek detail spesialis')

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - button card spesialis'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Explore/Learning/Learning - button card spesialis'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button back'), GlobalVariable.g_timeout)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - tab Latest'), GlobalVariable.g_timeout)

Mobile.verifyElementVisible(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.comment('explore journal')

Mobile.tap(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/More - button Journal'), 0)

Mobile.tap(findTestObject('Detail Spesialis/More - button Journal'), 0)

Mobile.comment('masuk ke detail journal kalau ada')

if (Mobile.waitForElementPresent(findTestObject('Feeds/Feed - text tag Journal'), 10, FailureHandling.OPTIONAL) == true) {
    Mobile.verifyElementVisible(findTestObject('Feeds/Feed - text tag Journal'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Feeds/Feed - text tag Journal'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text journal'), 0)

    Mobile.verifyElementVisible(findTestObject('Detil Journal/Journal - button back'), 0)

    Mobile.verifyElementVisible(findTestObject('Detil Journal/Journal - button more'), 0)

    judul_journal = Mobile.getText(findTestObject('Detil Journal/Journal - text judul jurnal'), 0).trim()

    GlobalVariable.g_judul_journal = judul_journal

    Mobile.comment('masuk ke PDF')

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button download PDF'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detil Journal/Journal - button download PDF'), 0)

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button close'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button download'), 0)

    Mobile.comment('download pdf')

    Mobile.tap(findTestObject('Detil Journal/Journal - button download'), 0)

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text file download success'), GlobalVariable.g_timeout)

    Mobile.comment('kembali ke detail journal')

    Mobile.tap(findTestObject('Detil Journal/Journal - button close'), 0)

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button more'), 0)

    Mobile.comment('bookmark')

    Mobile.tap(findTestObject('Detil Journal/Journal - button more'), 0)

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text bookmark'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detil Journal/Journal - text bookmark'), 0)

    Mobile.delay(4, FailureHandling.STOP_ON_FAILURE)

    //    not_run: Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text Bookmark success'), 0)
    //    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button more'), GlobalVariable.g_timeout)
    //
    //    Mobile.tap(findTestObject('Detil Journal/Journal - button more'), 0)
    //
    //    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text unbookmark'), GlobalVariable.g_timeout)
    //
    //    Mobile.tap(findTestObject('Detil Journal/Journal - text unbookmark'), 0)
    //    not_run: Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text Unbookmark success'), 0)
    //    Mobile.delay(4, FailureHandling.STOP_ON_FAILURE)
    Mobile.comment('kembali ke list journal')

    Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button back'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detil Journal/Journal - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button more'), GlobalVariable.g_timeout)
} else {
    println('Skip Karena Journal tidak ditemukan di Spesialis Ini')
}

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.comment('explore video')

Mobile.tap(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/More - button Video'), 0)

Mobile.tap(findTestObject('Detail Spesialis/More - button Video'), 0)

Mobile.comment('masuk ke detail video kalau ada')

if (Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - tag video'), 10, FailureHandling.OPTIONAL) == 
true) {
    Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - tag video'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detail Spesialis/Learning - tag video'), GlobalVariable.g_timeout)

    Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

    Mobile.pressBack()

    Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button more'), GlobalVariable.g_timeout)
} else {
    println('Skip Karena Video tidak ditemukan di Spesialis Ini')
}

Mobile.comment('explore guideline')

Mobile.tap(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/More - button Guideline'), 0)

Mobile.tap(findTestObject('Detail Spesialis/More - button Guideline'), 0)

Mobile.comment('masuk ke detail guideline kalau ada')

if (Mobile.waitForElementPresent(findTestObject('Feeds/Feed - button card Guideline'), 10, FailureHandling.OPTIONAL) == 
true) {
    Mobile.waitForElementPresent(findTestObject('Feeds/Feeds - text tag Guideline'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Feeds/Feeds - text tag Guideline'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - button close'), GlobalVariable.g_timeout)

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - text Guideline feeds'), GlobalVariable.g_timeout)

    Mobile.verifyElementVisible(findTestObject('Detail Guideline/Guideline - button download'), 0)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.comment('download pdf')

    Mobile.tap(findTestObject('Detail Guideline/Guideline - button download'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - File sukses download'), GlobalVariable.g_timeout)

    Mobile.comment('kembali ke list Feeds')

    Mobile.waitForElementPresent(findTestObject('Detail Guideline/Guideline - button close'), GlobalVariable.g_timeout)

    Mobile.tap(findTestObject('Detail Guideline/Guideline - button close'), 0)

    Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button more'), GlobalVariable.g_timeout)
} else {
    println('Skip Karena Guideline tidak ditemukan di Spesialis Ini')
}

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.tap(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/More - button All'), 0)

Mobile.tap(findTestObject('Detail Spesialis/More - button All'), 0)

Mobile.comment('unbookmark')

not_run: Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - tab Bookmark'), 0)

not_run: Mobile.tap(findTestObject('Detail Spesialis/Learning - tab Bookmark'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Feeds/Feed - text tag Journal'), 0)

not_run: Mobile.tap(findTestObject('Feeds/Feed - text tag Journal'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button more'), GlobalVariable.g_timeout)

not_run: Mobile.tap(findTestObject('Detil Journal/Journal - button more'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text unbookmark'), GlobalVariable.g_timeout)

not_run: Mobile.tap(findTestObject('Detil Journal/Journal - text unbookmark'), 0)

not_run: Mobile.delay(4, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button back'), 0)

not_run: Mobile.tap(findTestObject('Detil Journal/Journal - button back'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button search'), 0)

not_run: Mobile.tap(findTestObject('Detail Spesialis/Learning - tab Latest'), 0)

not_run: Mobile.tap(findTestObject('Detail Spesialis/Learning - tab Bookmark'), 0)

not_run: Mobile.verifyElementNotExist(findTestObject('Feeds/Feed - text tag Journal'), 4)

not_run: Mobile.tap(findTestObject('Detail Spesialis/Learning - tab Latest'), 0)

Mobile.comment('search konten spesialis')

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button search'), 0)

Mobile.tap(findTestObject('Detail Spesialis/Learning - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - input search konten'), 0)

Mobile.setText(findTestObject('Detail Spesialis/Learning - input search konten'), GlobalVariable.g_judul_journal, 0)

Mobile.comment('pilih tab journal')

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - search tab journal'), 0)

Mobile.tap(findTestObject('Detail Spesialis/Learning - search tab journal'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - judul jurnal search'), GlobalVariable.g_timeout)

not_run: Mobile.tap(findTestObject('Explore/Learning/Learning - judul jurnal search'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - button back'), 2)

not_run: Mobile.waitForElementPresent(findTestObject('Detil Journal/Journal - text judul jurnal'), 2)

not_run: Mobile.tap(findTestObject('Detil Journal/Journal - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button close search'), GlobalVariable.g_timeout)

Mobile.tap(findTestObject('Detail Spesialis/Learning - button close search'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button search'), 0)

Mobile.comment('request journal via spesialis')

Mobile.tap(findTestObject('Detail Spesialis/Learning - button more'), 0)

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/More - button Request Journal'), 0)

Mobile.tap(findTestObject('Detail Spesialis/More - button Request Journal'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - text Request Journal'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - choose city'), 0)

Mobile.tap(findTestObject('Explore/Request Journal/Request - choose city'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/City - text kabupaten aceh barat'), 0)

Mobile.tap(findTestObject('Explore/Request Journal/City - text kabupaten aceh barat'), 0)

if (Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - input title journal'), 5, FailureHandling.OPTIONAL) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)

    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - input title journal'), 0)

Mobile.setText(findTestObject('Explore/Request Journal/Request - input title journal'), v_judul_request_journal, 0)

if (Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - button Submit'), 5, FailureHandling.OPTIONAL) == 
false) {
    Mobile.swipe(startX, startY, endX, endY)

    Mobile.swipe(startX, startY, endX, endY)
}

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - button Submit'), 0)

Mobile.tap(findTestObject('Explore/Request Journal/Request - button Submit'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Request Journal/Request - text request journal berhasil'), 0)

if (Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - text Learning'), 5, FailureHandling.OPTIONAL) == 
false) {
    Mobile.comment('kembali ke list spesialis')

    Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button back'), 0)

    Mobile.tap(findTestObject('Detail Spesialis/Learning - button back'), 0)

    Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - text Learning'), GlobalVariable.g_timeout)
}

Mobile.comment('cari spesialis')

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - button search'), 0)

Mobile.tap(findTestObject('Explore/Learning/Learning - button search'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - input search'), 0)

Mobile.setText(findTestObject('Explore/Learning/Learning - input search'), v_spesialis, 0)

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - button card spesialis'), 0)

Mobile.tap(findTestObject('Explore/Learning/Learning - button card spesialis'), 0)

Mobile.comment('kembali ke list spesialis')

Mobile.waitForElementPresent(findTestObject('Detail Spesialis/Learning - button back'), 0)

Mobile.tap(findTestObject('Detail Spesialis/Learning - button back'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - button close search'), 0)

Mobile.tap(findTestObject('Explore/Learning/Learning - button close search'), 0)

Mobile.waitForElementPresent(findTestObject('Explore/Learning/Learning - text Learning'), 0)

